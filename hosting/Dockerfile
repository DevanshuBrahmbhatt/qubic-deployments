# Use a base image with Ubuntu
FROM ubuntu:latest

# Set non-interactive mode
ENV DEBIAN_FRONTEND=noninteractive

# Update package sources and install necessary packages
RUN apt-get update && apt-get install -y \
    git \
    python3 \
    python3-venv \
    curl \
    lsof \
    wget \
    npm \
    && rm -rf /var/lib/apt/lists/*

# Clone repositories
RUN git clone https://gitlab.com/DevanshuBrahmbhatt/qubic-data-storage.git /app/qubic-data-storage \
    && git clone https://gitlab.com/DevanshuBrahmbhatt/qubic-data-storage-frontend.git /app/qubic-data-storage-frontend

# Install Dolt
RUN curl -L https://github.com/dolthub/dolt/releases/latest/download/install.sh | bash \
    && useradd -r -m -d /var/lib/doltdb dolt

# Additional steps for Dolt setup
RUN dolt config --global --add user.email doltServer@company.com \
    && dolt config --global --add user.name "Dolt Server Account"

# Set up and configure MySQL
RUN apt-get update \
    && apt-get install -y lsb-release \  
    && wget http://repo.mysql.com/mysql-apt-config_0.8.26-1_all.deb \
    && dpkg -i mysql-apt-config_0.8.26-1_all.deb \
    && apt-get update \
    && apt-get install -y mysql-shell mysql-server

# Install required Python packages
RUN python3 -m venv /app/venv \
    && /app/venv/bin/pip3 install -r /app/qubic-data-storage/requirements.txt 

# Initialize Dolt repository and create the "calibration" database
 RUN cd /app/qubic-data-storage \
    && dolt init \
    && dolt sql -q "CREATE DATABASE calibration"


# Set working directory for frontend

WORKDIR /app/qubic-data-storage-frontend

# Install Node.js using NVM
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash \
    && . ~/.nvm/nvm.sh \
    && nvm install 14 \
    && nvm use 14 \
    && npm install -g yarn
ENV PATH="/usr/bin/versions/node/v14.21.3/bin:${PATH}"
# Build and run Vue.js frontend
RUN yarn install \
    && yarn build

# Expose the necessary ports
EXPOSE 3306 5000 8080

# Set working directory to qubic-data-storage
WORKDIR /app/qubic-data-storage

# Activate the virtual environment, run dolt sql-server, and start the services
CMD ["/bin/bash", "-c", "\
    source /app/venv/bin/activate && \
    nohup dolt sql-server -u root & \
    nohup /app/venv/bin/flask run --host=0.0.0.0 & \
    cd /app/qubic-data-storage-frontend/dist && nohup yarn preview --host=0.0.0.0"]
