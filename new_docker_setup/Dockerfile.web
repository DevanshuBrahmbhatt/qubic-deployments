# Use a base image with Ubuntu
FROM ubuntu:latest

# Set non-interactive mode
ENV DEBIAN_FRONTEND=noninteractive

# Update package sources and install necessary packages
RUN apt-get update && apt-get install -y \
    git \
    python3 \
    python3-venv \
    curl \
    lsof \
    wget \
    npm \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y supervisor \
    && rm -rf /var/lib/apt/lists/*

# Clone repositories
RUN git clone https://gitlab.com/DevanshuBrahmbhatt/qubic-data-storage.git /app/qubic-data-storage

# Install required Python packages
RUN python3 -m venv /app/venv \
    && /app/venv/bin/pip3 install -r /app/qubic-data-storage/requirements.txt 

# Set working directory for frontend
WORKDIR /app/qubic-data-storage/qubic-data-storage-frontend

# Install Node.js using NVM
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash \
    && . ~/.nvm/nvm.sh \
    && nvm install 14 \
    && nvm use 14 \
    && npm install -g yarn


RUN ls -la

ENV PATH="/usr/bin/versions/node/v14.21.3/bin:${PATH}"
# Build and run Vue.js frontend
RUN yarn install 
RUN yarn build

# Expose the necessary ports
EXPOSE 5000

# Set working directory to qubic-data-storage
WORKDIR /app/qubic-data-storage

# Copy supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Start processes
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
