# QubiC-Deployments

## Install
```bash
sudo apt-get update
sudo apt-get install docker.io
```

# Create Dockerfile
```
nano Dockerfile
```
// paste content 
```
ctrl+o
Enter
ctrl+x
```


## Build from Cache
```
sudo docker build -t my-qubic-app .  
```

## Build without Cache
```
sudo docker build --no-cache -t my-qubic-app .
```

## Run
```
sudo docker run -p 3306:3306 -p 5000:5000  my-qubic-app
```